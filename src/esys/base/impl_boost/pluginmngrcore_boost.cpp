/*!
 * \file esys/base/boost/pluginmngrcore_boost.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2022 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/base/esysbase_prec.h"
#include "esys/base/pluginbase.h"
#include "esys/base/impl_boost/pluginmngrcore.h"

#include <boost/filesystem.hpp>
#include <boost/dll/runtime_symbol_info.hpp>

#include <iostream>

namespace esys::base::impl_boost
{

PluginMngrImplHelper::PluginMngrImplHelper() = default;

PluginMngrImplHelper::~PluginMngrImplHelper() = default;

void PluginMngrImplHelper::set_dyn_lib(std::shared_ptr<DynLibrary> &dyn_lib)
{
    m_dyn_lib = dyn_lib;
}

DynLibrary *PluginMngrImplHelper::get_dyn_lib()
{
    return m_dyn_lib.get();
}

void PluginMngrImplHelper::set_entry_point(void *entry)
{
    m_entry_fct = entry;
}

void *PluginMngrImplHelper::get_entry_point()
{
    return m_entry_fct;
}

void PluginMngrImplHelper::set_plugin(PluginBase *plugin)
{
    m_plugin = plugin;
}

PluginBase *PluginMngrImplHelper::get_plugin()
{
    return m_plugin;
}

PluginMngrCore::PluginMngrCore(const std::string &name)
    : PluginMngrBase(name)
{
}

PluginMngrCore::~PluginMngrCore() = default;

int PluginMngrCore::load(const std::string &dir)
{
    std::shared_ptr<PluginMngrImplHelper> helper;
    PluginBase *plugin;
    auto plugin_lib = std::make_shared<DynLibrary>();

    if (plugin_lib->load(dir) != 0)
    {
        if (get_verbose_level() > 0)
        {
            std::cout << "    failed to load the plugin." << std::endl;
        }
        return -1;
    }
    if (plugin_lib->has_symbol(get_entry_fct_name()) == false)
    {
        if (get_verbose_level() > 0)
        {
            std::cout << "    doesn't have entry fct '" << get_entry_fct_name() << "'." << std::endl;
        }
        return -2;
    }

    void *plugin_entry_function = plugin_lib->get_symbol(get_entry_fct_name());
    if (plugin_entry_function == nullptr)
    {
        if (get_verbose_level() > 0)
        {
            std::cout << "    failed to get the entry fct '" << get_entry_fct_name() << "'." << std::endl;
        }
        return -3;
    }

    plugin = get_plugin_from_entry_fct(plugin_entry_function);

#ifdef _MSC_VER
#ifdef _DEBUG
    if (plugin->is_debug() == false)
    {
        if (get_verbose_level() > 0)
        {
            std::cout << "    plugin is a release build." << std::endl;
        }
        return -4;
    }
#elif NDEBUG
    if (plugin->is_debug() == true)
    {
        if (get_verbose_level() > 0)
        {
            std::cout << "    plugin is a debug build." << std::endl;
        }
        return -5;
    }
#else
#error _DEBUG or NDEBUG must be defined
#endif
#endif
    helper = std::make_shared<PluginMngrImplHelper>();

    helper->set_dyn_lib(plugin_lib);
    helper->set_entry_point(plugin_entry_function);
    helper->set_plugin(plugin);
    set_plugin_filename(plugin, dir);

    m_plugins.push_back(helper);
    if (get_verbose_level() > 0)
    {
        std::cout << "    plugin loaded successfully." << std::endl;
    }
    return 0;
}

int PluginMngrCore::load()
{
    if (get_is_loaded()) return -1;

    int result = 0;
    std::string abs_plugin_dir;
    boost::filesystem::directory_iterator file_end_it;
    DynLibrary *plugin_lib = nullptr;

    result = find_plugin_folder(abs_plugin_dir);
    if (result < 0)
    {
        if (get_verbose_level() > 0)
        {
            std::cout << "Couldn't deduce the plugin folder." << std::endl;
        }
        return result;
    }
    if (abs_plugin_dir.empty())
    {
        if (get_verbose_level() > 0)
        {
            std::cout << "Found plugin folder is empty." << std::endl;
        }
        return -2;
    }

#ifdef _MSC_VER
#ifdef _DEBUG

    if (get_verbose_level() > 0)
    {
        std::cout << "Build type    = debug" << std::endl;
    }

#elif NDEBUG
    if (get_verbose_level() > 0)
    {
        std::cout << "Build type    = release" << std::endl;
    }
#else
    if (get_verbose_level() > 0)
    {
        std::cout << "Build type    = ?" << std::endl;
    }
#endif
#endif

    if (get_verbose_level() > 0)
    {
        std::cout << "Plugin folder = " << abs_plugin_dir.c_str() << std::endl;
    }
#ifdef WIN32
    SetDllDirectory(abs_plugin_dir.c_str());
#endif

#ifdef WIN32
    std::string mask = "*.dll";
#else
    std::string mask = "*.so";
#endif
    boost::filesystem::path search_path = abs_plugin_dir;
    search_path /= mask;

    if (get_verbose_level() > 0)
    {
        std::cout << "Search path   = " << search_path.string() << std::endl << std::endl;
    }

    for (boost::filesystem::directory_iterator it(abs_plugin_dir); it != file_end_it; ++it)
    {
        if (get_verbose_level() > 0)
        {
            std::cout << "Test file = " << it->path().string() << std::endl;
        }
        // If it's not a directory, list it. If you want to list directories too, just remove this check.
        if (!boost::filesystem::is_regular_file(it->path()))
        {
            if (get_verbose_level() > 0)
            {
                std::cout << "    not a regular file." << std::endl;
            }
            continue;
        }
        if (boost::filesystem::is_symlink(it->path()))
        {
            if (get_verbose_level() > 0)
            {
                std::cout << "    is a symlink." << std::endl;
            }
            continue;
        }

        // assign current file name to current_file and echo it out to the console.
        std::string current_file = it->path().string();
        result = load(current_file);
    }
    set_is_loaded(true);

    if (get_verbose_level() > 0)
    {
        std::cout << std::endl;
    }

    if (!m_plugins.empty()) return 0;
    return -1;
}

int PluginMngrCore::release()
{
    uint16_t idx;

    for (idx = 0; idx < m_plugins.size(); ++idx)
    {
        assert(m_plugins[idx]->get_plugin() != nullptr);

        m_plugins[idx]->get_plugin()->release();
        m_plugins[idx].reset();
    }
    m_plugins.clear();
    set_is_loaded(false);

    return 0;
}

std::size_t PluginMngrCore::get_size()
{
    return m_plugins.size();
}

PluginBase *PluginMngrCore::get_base(std::size_t index)
{
    if (index >= m_plugins.size()) return nullptr;
    return m_plugins[index]->get_plugin();
}

int PluginMngrCore::find_exe_path(std::string &exe_path)
{
    return s_find_exe_path(exe_path);
}

int PluginMngrCore::find_plugin_folder(std::string &plugin_folder)
{
    int result = 0;
    boost::filesystem::path plugin_dir;

    std::vector<std::string> search_paths;

    if (get_search_folder().empty())
    {
        boost::filesystem::path search_path;
        std::string path;
        result = find_exe_path(path);
        if (result < 0) return -2;
        search_path = path;
        search_path = search_path.parent_path(); // Remove the executable name
#ifndef WIN32
        // To get to the parent of bin folder
        search_path = search_path.parent_path();
#endif
        search_paths.push_back(search_path.string());
    }
    else
        search_paths.push_back(get_search_folder());

    char const *value = nullptr;

    for (auto const &env_var_search_path : get_env_var_search_folders())
    {
        value = std::getenv(env_var_search_path.c_str());
        if (value == nullptr) continue;
        search_paths.push_back(value);
    }

    if (!get_base_folder().empty())
    {
        search_paths.push_back(get_base_folder());
    }

    boost::filesystem::path path_to_test;

    std::string rel_plugin_path;
    result = get_rel_plugin_path(rel_plugin_path);
    if (result < 0) return -3;

    for (auto const &search_path : search_paths)
    {
        path_to_test = search_path;
        path_to_test /= rel_plugin_path;
        path_to_test = ::boost::filesystem::absolute(path_to_test).normalize().make_preferred();
        if (boost::filesystem::exists(path_to_test))
        {
            plugin_folder = path_to_test.string();
            return 0;
        }
    }
    return -1;
}

int PluginMngrCore::s_find_exe_path(std::string &exe_path)
{
    boost::filesystem::path p;
    boost::system::error_code ec;

    p = boost::dll::program_location(ec);
    if (ec) return -1;

    exe_path = p.normalize().make_preferred().string();
    return 0;
}

} // namespace esys::base::impl_boost
